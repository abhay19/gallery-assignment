package madocs.skapps.com.assignmentapplication.adapter

import madocs.skapps.com.assignmentapplication.model.GalleryItem

interface ViewHolderClickListner {

    fun onClick(item:GalleryItem)
}