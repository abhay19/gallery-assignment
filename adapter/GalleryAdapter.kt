package madocs.skapps.com.assignmentapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import madocs.skapps.com.assignmentapplication.R
import madocs.skapps.com.assignmentapplication.model.GalleryItem

class GalleryAdapter(private val list:ArrayList<GalleryItem>, private val context: Context, private val listner: ViewHolderClickListner): RecyclerView.Adapter<GalleryAdapter.GalleryItemViewHolder>() {


    inner class GalleryItemViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
            val imageView = itemView.findViewById<ImageView>(R.id.iv_thumbnail)

        fun onBind(item: GalleryItem){
            Glide.with(context)
                .asBitmap()
                .load(item.thumbnailUrl+".jpg")
                .override(200,200)
                .into(imageView)
            itemView.tag = item
            itemView.setOnClickListener {
                listner.onClick(it.tag as GalleryItem)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.gallery_item, parent, false)
            return GalleryItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: GalleryItemViewHolder, position: Int) {
           holder.onBind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}