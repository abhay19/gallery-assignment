package madocs.skapps.com.assignmentapplication.api

import madocs.skapps.com.assignmentapplication.model.GalleryItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.GET

interface ApiService {
    @GET("photos")
    fun getPhotos():Call<ArrayList<GalleryItem>>
}