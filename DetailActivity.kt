package madocs.skapps.com.assignmentapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val title = findViewById<TextView>(R.id.title)
        val image = findViewById<ImageView>(R.id.image)
        val bundle = intent.extras
        title.text = bundle?.getString("title")
        Glide.with(this)
            .load(bundle?.getString("image")+".jpg")
            .timeout(60000)
            .into(image)
    }
}