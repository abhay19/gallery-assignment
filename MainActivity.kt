package madocs.skapps.com.assignmentapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.CallScreeningService
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import madocs.skapps.com.assignmentapplication.adapter.GalleryAdapter
import madocs.skapps.com.assignmentapplication.adapter.ViewHolderClickListner
import madocs.skapps.com.assignmentapplication.api.ApiService
import madocs.skapps.com.assignmentapplication.model.GalleryItem
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), ViewHolderClickListner {
    var apiService:ApiService? = null
    var recyclerView:RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.gallery)
        recyclerView?.layoutManager = GridLayoutManager(this,  2)
        createRetrofitClient()
        getGalleryPhotos()
    }

    private fun createRetrofitClient(){
        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    private fun getGalleryPhotos(){
        val call = apiService?.getPhotos()
        call?.enqueue(object:Callback<ArrayList<GalleryItem>>{
            override fun onResponse(
                call: Call<ArrayList<GalleryItem>>,
                response: Response<ArrayList<GalleryItem>>
            ) {
                val list = response.body()

                val adapter = GalleryAdapter(list?.slice(0..20) as ArrayList<GalleryItem>, this@MainActivity, this@MainActivity)
                recyclerView?.adapter = adapter
            }

            override fun onFailure(call: Call<ArrayList<GalleryItem>>, t: Throwable) {

            }

        })
    }

    override fun onClick(item:GalleryItem) {
        val intent = Intent(this,DetailActivity::class.java)
        intent.putExtra("title", item.title)
        intent.putExtra("image", item.url)
        startActivity(intent)
    }
}